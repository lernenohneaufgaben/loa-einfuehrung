---
author: Anne Peters
title: Lernen ohne Aufgaben
date: 2019
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

# Kinder 

----

## lernen ohne Aufgaben. 

- Ständig und überall.
- Wenn man sie lässt.
- In lernfreudiger Umgebung.
- Die Motivation ist angeboren.

# Wir 

----

## lernen ohne Aufgaben.

- Im Familien- und Freundeskreis.
- Wenn Interesse geweckt ist.
- Aus Freude, mit Begeisterung.
- Wir fühlen Zugehörigkeit. 

---

# Verhalten informell
## unter Freunden:

- Wir trauen uns, alles zu fragen.
- Wir zeigen uns, wie es geht.
- Stressfrei, weil im Vertrauen.
- Wir lernen an Fehlern.


---

# Verhalten formell
## in Schule oder Kurs:

- Wir stellen Aufgaben. (Lehrer*)
- Wir lösen Aufgaben. (Schüler*)
- Stressig, weil unter Bewertung. 
- Wir empfinden es als Pflicht. 
- Wir verbergen Schwächen.

---

# Wirkungsziele 
## LOA-Lernansatz:

- Informelles Verhalten in formelle Zusammenhänge bringen.
- Das "innere Lernkind" wiederentdecken.
- Trennung von Schule und Freizeit aufheben.
- Eine neue Lernbeziehungskultur.

---

# LOA-Wirkungsfelder

- 1. Fachthemen / Fachbereiche
- 2. Beziehungskultur in Lerngruppen
- 3. Individuelles Lerncoaching

---

# 1. Fachthemen / Fachbereiche

- Deutsch für Einsteiger
- Deutsch für Aufsteiger
- Mathematik und Sprache
- Freizeit und Sprache

----

## Deutsch für Einsteiger

- Aktiv aneignen mit Sprachschleifen.
- Komplexe Sprache für Einsteiger.
- Mentorenprinzip "Lektorat".
- Schrift-Coaching.

----

## Deutsch für Aufsteiger

- Grammatik um Mitternacht.
- 10 kleine Sprachexperten.
- Aktiv aneignen mit Merksprüchen.
- Variieren, improvisieren, frei verwenden.

----

## Mathematik und Sprache

- Statt Aufgaben aktives Handeln.
- Spielerisch durch Abstraktionsstufen.
- Gesten, Sprüche, innere Bilder.
- Zahlenschrift als Abbild von Welt.

----

## Freizeit und Sprache

- Ideen für aktive Freizeit.
- Handlungsbegleitendes Sprechen.
- Handlungsbegleitendes Schreiben.
 
---

# 2. Beziehungskultur in Lerngruppen

----

# Beziehungskultur in Lerngruppen

- 5 Lernstufen für Peerlernen.
- Befindlichkeits-Feedback.
- Vertrauen im kooperativen Lernmodus.
- Leistungsschere kollaborativ nutzen.

---

# 3. Individuelles Lerncoaching

----

# Individuelles Lerncoaching

- Unbewusste Prozesse wahrnehmen lernen.
- Koordination komplexer Abläufe.
- Lernhemmnisse überwinden.
- Lernwege mündig, selbsttätig gestalten.

---

## LOA für uns alle

- Bildung verschenken.
- Kultur des Teilens.
- Open Educational Ressources.

---

# Danke !

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.

Namensnennung so:
"LOA-Lernen-ohne-Aufgaben"

---

# LOA-Lernzirkel für Lehramtsstudenten*

- ab WS 2019
- 1x monatlich studienbegleitendes Coaching
- LOA-Lernen selbst erleben
- LOA-Elemente im Praxissemester anwenden

info@LOA.jetzt

---
